package com.intelyCare.demo.repository;

import com.intelyCare.demo.entities.Documents;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DocumentsRepo extends JpaRepository<Documents, Integer> {

    @Query(value = "select d from Documents d where (d.content like %?1% or d.content like %?2%) and d.content like %?3%")
    List<Documents> findByTokens(String token1, String token2, String token3);

    @Query(value = "select d from Documents d where d.content like %?1%")
    List<Documents> findAllByContent (String content);
}
