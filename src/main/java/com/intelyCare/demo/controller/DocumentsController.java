package com.intelyCare.demo.controller;

import com.intelyCare.demo.entities.Documents;
import com.intelyCare.demo.sevices.DocumentsService;
import javassist.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@Controller
@CrossOrigin(origins = "*")
@RequestMapping("/document")
public class DocumentsController {

    private final DocumentsService documentsService;


    public DocumentsController(DocumentsService documentsService) {
        this.documentsService = documentsService;
    }

    @PostMapping("/create")
    public ResponseEntity<Documents> createDocument(@RequestParam final String content) {
        return ResponseEntity.ok(this.documentsService.createDocument(content));
    }

    @GetMapping("/find")
    public ResponseEntity<Set<Documents>> findDocuments(@RequestParam final String token) {
        return ResponseEntity.ok(this.documentsService.findToken(token));
    }
    @GetMapping("/query")
    public ResponseEntity<List<Documents>> findWords(@RequestParam final String token1, @RequestParam final String token2, @RequestParam final String token3){
        return ResponseEntity.ok(this.documentsService.findWords(token1, token2, token3));
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Documents> updateDocument(@PathVariable("id") final Integer id, @RequestParam final String content) throws NotFoundException {
        return ResponseEntity.ok(this.documentsService.updateDocument(id, content));
    }

    @GetMapping("/all")
    public ResponseEntity<List<Documents>> getAll(){
        return ResponseEntity.ok(documentsService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Documents> getById(@PathVariable("id") final Integer id) throws NotFoundException {
        return ResponseEntity.ok(documentsService.getById(id));
    }

    @DeleteMapping("/delete/{id}")
    public void deleteDocument(@PathVariable(name = "id") final Integer id){
       documentsService.delete(id);
    }
}
