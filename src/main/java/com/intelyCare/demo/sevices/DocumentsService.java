package com.intelyCare.demo.sevices;

import com.intelyCare.demo.Errors.ErrorMessages;
import com.intelyCare.demo.entities.Documents;
import com.intelyCare.demo.repository.DocumentsRepo;
import javassist.NotFoundException;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class DocumentsService {

    private final DocumentsRepo documentsRepo;

    public DocumentsService(DocumentsRepo documentsRepo) {
        this.documentsRepo = documentsRepo;
    }

    public Documents createDocument(final String content) {
        Documents documents = new Documents();
        documents.setContent(content);
        return this.documentsRepo.save(documents);
    }

    public Set<Documents> findToken(final String token) {
        Set<Documents> foundDocuments = new HashSet<>();
        String[] tokens = token.split(" ");
        for (String tokenWord : tokens
        ) {
            foundDocuments.addAll(documentsRepo.findAllByContent(tokenWord));
        }
        return foundDocuments.stream().sorted(Comparator.comparing(Documents::getId)).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public List<Documents> findWords(final String token1, final String token2, final String token3){
        return this.documentsRepo.findByTokens(token1, token2, token3);
    }

    public Documents updateDocument(final Integer id, final String content) throws NotFoundException {
        Documents document = this.documentsRepo.findById(id).orElseThrow(() -> new NotFoundException(ErrorMessages.ID_NOT_FOUND_EXCEPTION));
        document.setContent(content);
        return documentsRepo.save(document);
    }

    public List<Documents> getAll(){
        return documentsRepo.findAll();
    }

    public Documents getById(Integer id) throws NotFoundException {
        return documentsRepo.findById(id).orElseThrow(()-> new NotFoundException(ErrorMessages.ID_NOT_FOUND_EXCEPTION));
    }

    @SneakyThrows
    public void delete(Integer id){
        Optional<Documents> document = documentsRepo.findById(id);
        if (document.isPresent()){
            documentsRepo.delete(document.get());
        } else throw new NotFoundException(ErrorMessages.ID_NOT_FOUND_EXCEPTION);
    }
}
